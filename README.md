# Caso práctico de la asignatura Visualización de Datos

Se pide realizar un dashboard interactivo con una visualización de cierto dataset. En este caso he elegido el [Barcelona datasets](https://www.kaggle.com/xvivancos/barcelona-data-sets).


Una versión pública del dashboard se encuentra en [https://juandspy-barcelona-dashboard.herokuapp.com](https://juandspy-barcelona-dashboard.herokuapp.com). He utilizado la versión gratuita de [Heroku](https://www.heroku.com/) para alojarlo, de ahí el bajo rendimiento. Se invita a todo aquel que quiera probar el dashboard en profundidad a seguir las instrucciones de instalación de la carpeta [App](./App) para poder levantar la web en local.

En [seguimiento.md](seguimiento.md) se lleva un registro de lo que se hace cada día que se complementa con los _commits_.
