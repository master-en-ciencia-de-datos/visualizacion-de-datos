# Seguimiento del desarrollo del caso práctico

### Respuesta de la propuesta:

La respuesta a la propuesta del dataset fue:

- Se tiene claro el conjunto de datos a utilizar. 

El dataset de datos demográficos de Barcelona.
- Se proponen varios objetivos, alguno de ellos no es un objetivo (probar distintos tipos de gráficos). 

    - [ ] Comprobar si el desempleo fomenta la emigración.
    - [ ] Investigar si existe relación entre la contaminación del aire y la mortalidad, aunque seguramente
    influyan muchos más factores.
    - [ ] Tratar de relacionar accidentes con los transportes y la mortalidad.

- Se recomienda fijar uno o dos objetivos específicos y, a partir de ahí, tener filtrados, subobjetivos adicionales. Pero tener claro que se quiere conseguir con la visualización. 

    - [ ] Visualización de cada CSV.
    - [ ] Buscar alguna correlación entre CSVs.
    
- También se recomienda PyViz para gráficos interactivos en el dashboard a desarrollar. 

    - [ ] Probar distintos tipos de gráficos, como nubes de palabras con los nombres de los habitantes.
    
- Se puede realizar una infografía como paso final, pero lo más relevante es el dashboard. 

    - [ ] Por ejemplo poner dibujos de autobuses y trenes para representar los usos del transporte público.

### Entrega intermedia

En la entrega intermedia deberéis entregar un fichero comprimido con lo siguiente:

- el conjunto de datos procesado

- la memoria con el proceso seguido para la adquisición y análisis de los datos,

- un primer borrador de la memoria donde se describan los objetivos en los que planteáis focalizar la visualización y las visualizaciones que se derivan de éstos y del análisis realizado. Es importante recordar que esta asignatura se centra en el análisis de la visualización de datos orientada a un objetivo, por tanto, se deberá hacer énfasis en este punto, y no tanto en los modelos matemáticos de los que se derivan los datos.

Tened en cuenta también que en esta entrega intermedia podéis tener ciertas deficiencias de formato, pero la memoria final debe estar bien redactada y formateada.  Por tanto, haced el favor de resumir en unas líneas (un párrafo o dos) el estado de las distintas secciones entregadas (terminada, falta detallar X, tengo dudas con esta parte porque Y, etc. ). Al menos enviad una sección terminada para tener una idea del resultado esperado al final. De esta manera nuestros comentarios se centrarán en puntualizaciones que os puedan ser más útiles.

#### Exploración del dataset

##### 09/04/2020
Descripción del dataset en [este archivo](Dataset/docs/descripcion.md).

##### 10/04/2020

Comienzo a hacer notebook en [esta carpeta](Dataset/scripts). Creo la página de la wiki de [births.csv](https://gitlab.com/master-en-ciencia-de-datos/visualizacion-de-datos/-/wikis/analisis-exploratorio/births.csv).

##### 11/04/2020

Termino el [analisis-exploratorio-births.ipynb](/home/juan/Documentos/Master/Visualización de datos/Dataset/scripts/analisis-exploratorio-births.ipynb). Empiezo a poner los resultados de este notebook en un Dash como [este](https://dash-gallery.plotly.host/dash-opioid-epidemic/). Primero lo hago por barrios, pero sería interesante incluir un checkbox para ponerlo por distritos.

Va a haber que crear un [CSV](/home/juan/Documentos/Master/Visualización de datos/App/data/lat_lon_counties.csv) con los datos de los distritos/barrios.

##### 12/04/2020

Plotly no tiene mapa de Barcelona con los barrios. Hay que aplazarlo hasta nuevo aviso. Voy a centrarme en el objetivo de relacionar emigración con paro. Lo primero es poner un [gráfico Circos](https://dash.plotly.com/dash-bio/circos) para ver el flujo de emigración. Hago el análisis exploratorio en [immigrants-emigrants-by-destination.ipynb](/home/juan/Documentos/Master/Visualización de datos/Dataset/scripts/immigrants-emigrants-by-destination.ipynb). Que usa el CSV [immigrants_emigrants_by_destination.csv](/home/juan/Documentos/Master/Visualización de datos/Dataset/files/immigrants_emigrants_by_destination.csv) y [immigrants_emigrants_by_destination2.csv](/home/juan/Documentos/Master/Visualización de datos/Dataset/files/immigrants_emigrants_by_destination2.csv).

##### 18/04/2020

Creo mapa interactivo con emigraciones e inmigraciones en [app.py](/home/juan/Documentos/Master/Visualización de datos/App/emigrations/app.py). Uso este [mapa](https://laura-an.carto.com/tables/shapefile_distrito_barcelona/public/map).
El siguiente paso será añadir la opción de usar las emigraciones e inmigraciones
a Barcelona y desde Barcelona hacia las CCAA españolas.

##### 24/04/2020

Inicio la memoria de adquisición y análisis de datos. Añado "Desempleo" al dahboard de emigraciones. 

##### 25/04/2020

Añado gráfico con posibilidad de elegir las barras y líneas entre emigración, inmigración y desempleo.

##### 02/05/2020

Cierro la parte de la memoria de emigrations y empiezo con la del transporte. 
El tema de la contaminación no se va a poder estudiar porque solo hay datos de
noviembre de 2017.

El resto de cambios se dejan reflejados en los mensajes de los _commits_.