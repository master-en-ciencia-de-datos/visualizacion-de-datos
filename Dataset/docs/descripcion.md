# Descripción del dataset

##### Demografía

- _births.csv_: Nacimientos por nacionalidad y vecindario (2013-2017).

| Year 	| District Code 	| District Name 	| Neighborhood Code 	| Neighborhood Name 	| Gender 	| Number 	|
|------	|---------------	|---------------	|-------------------	|-------------------	|--------	|--------	|
| 2017 	| 1             	| Ciutat Vella  	| 1                 	| el Raval          	| Boys   	| 283    	|
| 2017 	| 1             	| Ciutat Vella  	| 2                 	| el Barri Gòtic    	| Boys   	| 56     	|
| 2017 	| 1             	| Ciutat Vella  	| 3                 	| la Barceloneta    	| Boys   	| 51     	|

- _deaths.csv_: Muertes en edades quinquenales (grupos de cinco años) y vecindarios (2015-2017).

| Year 	| District.Code 	| District.Name 	| Neighborhood.Code 	| Neighborhood.Name 	| Age 	| Number 	|
|------	|---------------	|---------------	|-------------------	|-------------------	|-----	|--------	|
| 2017 	| 1             	| Ciutat Vella  	| 1                 	| el Raval          	| 0-4 	| 1      	|
| 2017 	| 1             	| Ciutat Vella  	| 2                 	| el Barri Gòtic    	| 0-4 	| 1      	|
| 2017 	| 1             	| Ciutat Vella  	| 3                 	| la Barceloneta    	| 0-4 	| 0      	|

- _population.csv_: Población por barrio, edades quinquenales y género (2013-2017).

| Year 	| District.Code 	| District.Name 	| Neighborhood.Code 	| Neighborhood.Name 	| Gender 	| Age 	| Number 	|
|------	|---------------	|---------------	|-------------------	|-------------------	|--------	|-----	|--------	|
| 2017 	| 1             	| Ciutat Vella  	| 1                 	| el Raval          	| Male   	| 0-4 	| 224    	|
| 2017 	| 1             	| Ciutat Vella  	| 2                 	| el Barri Gòtic    	| Male   	| 0-4 	| 50     	|
| 2017 	| 1             	| Ciutat Vella  	| 3                 	| la Barceloneta    	| Male   	| 0-4 	| 43     	|

- _unemployment.csv_: Desempleo por distrito, barrio y género (2013-2017).

| Year 	| Month   	| District Code 	| District Name 	| Neighborhood Code 	| Neighborhood Name 	| Gender 	| Demand_occupation     	| Number 	|
|------	|---------	|---------------	|---------------	|-------------------	|-------------------	|--------	|-----------------------	|--------	|
| 2017 	| January 	| 1             	| Ciutat Vella  	| 1                 	| el Raval          	| Male   	| Registered unemployed 	| 2107   	|
| 2017 	| January 	| 1             	| Ciutat Vella  	| 2                 	| el Barri Gòtic    	| Male   	| Registered unemployed 	| 538    	|
| 2017 	| January 	| 1             	| Ciutat Vella  	| 3                 	| la Barceloneta    	| Male   	| Registered unemployed 	| 537    	|

- _immigrants_by_nationality.csv_: inmigrantes por nacionalidad y barrio (2015-2017).

| Year 	| District Code 	| District Name 	| Neighborhood Code 	| Neighborhood Name 	| Nationality 	| Number 	|
|------	|---------------	|---------------	|-------------------	|-------------------	|-------------	|--------	|
| 2017 	| 1             	| Ciutat Vella  	| 1                 	| el Raval          	| Spain       	| 1109   	|
| 2017 	| 1             	| Ciutat Vella  	| 2                 	| el Barri Gòtic    	| Spain       	| 482    	|
| 2017 	| 1             	| Ciutat Vella  	| 3                 	| la Barceloneta    	| Spain       	| 414    	|

- _immigrants_emigrants_by_age.csv_: inmigrantes y emigrantes por edades quinquenales y barrio (2015-2017).

| Year 	| District Code 	| District Name 	| Neighborhood Code 	| Neighborhood Name 	| Age 	| Immigrants 	| Emigrants 	|
|------	|---------------	|---------------	|-------------------	|-------------------	|-----	|------------	|-----------	|
| 2017 	| 1             	| Ciutat Vella  	| 1                 	| el Raval          	| 0-4 	| 154        	| 108       	|
| 2017 	| 1             	| Ciutat Vella  	| 2                 	| el Barri Gòtic    	| 0-4 	| 58         	| 33        	|
| 2017 	| 1             	| Ciutat Vella  	| 3                 	| la Barceloneta    	| 0-4 	| 38         	| 37        	|

- _immigrants_emigrants_by_destination.csv_: inmigrantes y emigrantes por lugar origen y destino a/desde Barcelona (2017).

| from      	| to        	| weight 	|
|-----------	|-----------	|--------	|
| Abroad    	| Barcelona 	| 53085  	|
| Andalusia 	| Barcelona 	| 2416   	|
| Aragon    	| Barcelona 	| 811    	|

- _immigrants_emigrants_by_destination2.csv_: inmigrantes y emigrantes por lugar de origen y destino, respectivamente, a/desde los distritos de Barcelona (2017).

| from         	| to             	| weight 	|
|--------------	|----------------	|--------	|
| Ciutat Vella 	| Andalusia      	| 188    	|
| Ciutat Vella 	| Aragon         	| 64     	|
| Ciutat Vella 	| Canary Islands 	| 95     	|

- _immigrants_emigrants_by_sex.csv_: inmigrantes y emigrantes por género y barrio (2013-2017).

| Year 	| District Code 	| District Name 	| Neighborhood Code 	| Neighborhood Name 	| Gender 	| Immigrants 	| Emigrants 	|
|------	|---------------	|---------------	|-------------------	|-------------------	|--------	|------------	|-----------	|
| 2017 	| 1             	| Ciutat Vella  	| 1                 	| el Raval          	| Male   	| 3063       	| 1195      	|
| 2017 	| 1             	| Ciutat Vella  	| 2                 	| el Barri Gòtic    	| Male   	| 1499       	| 427       	|
| 2017 	| 1             	| Ciutat Vella  	| 3                 	| la Barceloneta    	| Male   	| 910        	| 361       	|

- _most_frequent_baby_names.csv_: 25 nombres más comunes de Barcelona separados por género (1996-2016).

| Order 	| Name  	| Gender 	| Year 	| Frequency 	|
|-------	|-------	|--------	|------	|-----------	|
| 1     	| LAURA 	| Female 	| 1996 	| 237       	|
| 2     	| MARIA 	| Female 	| 1996 	| 219       	|
| 3     	| MARTA 	| Female 	| 1996 	| 206       	|

- _most_frequent_names.csv_: 50 nombres más comunes de los habitantes de Barcelona separados por género.

| Order 	| Name   	| Gender 	| Decade      	| Frequency 	|
|-------	|--------	|--------	|-------------	|-----------	|
| 1     	| MARIA  	| Female 	| Before 1930 	| 2085      	|
| 2     	| CARMEN 	| Female 	| Before 1930 	| 1463      	|
| 3     	| JOSEFA 	| Female 	| Before 1930 	| 1032      	|

- _life_expectancy.csv_: esperanza de vida en hombres y mujeres (2006-2013).

| Neighborhood   	| 2006-2010 	| 2007-2011 	| 2008-2012 	| 2009-2013 	| 2010-2014 	| Gender 	|
|----------------	|-----------	|-----------	|-----------	|-----------	|-----------	|--------	|
| el Raval       	| 87.5      	| 84.9      	| 84.7      	| 84.9      	| 85.3      	| Female 	|
| el Barri Gòtic 	| 88        	| 84.3      	| 84.4      	| 87.5      	| 84.4      	| Female 	|
| la Barceloneta 	| 88.2      	| 85.3      	| 84.4      	| 86.3      	| 84.7      	| Female 	|

##### Accidents

- _accidents_2017.csv_: lista de accidentes gestionados por la policía local. Incluye número de lesiones por gravedad, número de vehículos involucrados y lugar del accidente.

| Id          	| District Name 	| Neighborhood Name 	| Street                                       	| Weekday 	| Month     	| Day 	| Hour 	| Part of the day 	| Mild injuries 	| Serious injuries 	| Victims 	| Vehicles involved 	| Longitude  	| Latitude    	|
|-------------	|---------------	|-------------------	|----------------------------------------------	|---------	|-----------	|-----	|------	|-----------------	|---------------	|------------------	|---------	|-------------------	|------------	|-------------	|
| 2017S008429 	| Unknown       	| Unknown           	| Número 27                                    	| Friday  	| October   	| 13  	| 8    	| Morning         	| 2             	| 0                	| 2       	| 2                 	| 2.12562442 	| 41.34004482 	|
| 2017S007316 	| Unknown       	| Unknown           	| Número 3 Zona Franca / Número 50 Zona Franca 	| Friday  	| September 	| 1   	| 13   	| Morning         	| 2             	| 0                	| 2       	| 2                 	| 2.12045245 	| 41.33942606 	|
| 2017S010210 	| Unknown       	| Unknown           	| Litoral (Besòs)                              	| Friday  	| December  	| 8   	| 21   	| Afternoon       	| 5             	| 0                	| 5       	| 2                 	| 2.1673561  	| 41.3608855  	|

##### Environment

- _air_quality_Nov2017.csv_: calidad del aire. Medidas de O3 (Ozono en la troposfera), NO2 (Dióxido de Nitrógeno), PM10 (Partículas en suspensión).

| Station              	| Air Quality 	| Longitude 	| Latitude 	| O3 Hour 	| O3 Quality 	| O3 Value 	| NO2 Hour 	| NO2 Quality 	| NO2 Value 	| PM10 Hour 	| PM10 Quality 	| PM10 Value 	| Generated       	| Date Time  	|
|----------------------	|-------------	|-----------	|----------	|---------	|------------	|----------	|----------	|-------------	|-----------	|-----------	|--------------	|------------	|-----------------	|------------	|
| Barcelona - Sants    	| Good        	| 2.1331    	| 41.3788  	| NA      	| NA         	| NA       	| 0h       	| Good        	| 84        	| NA        	| NA           	| NA         	| 01/11/2018 0:00 	| 1541027104 	|
| Barcelona - Eixample 	| Moderate    	| 2.1538    	| 41.3853  	| 0h      	| Good       	| 1        	| 0h       	| Moderate    	| 113       	| 0h        	| Good         	| 36         	| 01/11/2018 0:00 	| 1541027104 	|
| Barcelona - Gràcia   	| Good        	| 2.1534    	| 41.3987  	| 0h      	| Good       	| 10       	| 0h       	| Good        	| 73        	| NA        	| NA           	| NA         	| 01/11/2018 0:00 	| 1541027104 	|

- _air_stations_Nov2017.csv_: estaciones de medida de la calidad del aire.

| Station                	| Longitude 	| Latitude 	| Ubication                                                  	| District Name 	| Neighborhood Name                     	|
|------------------------	|-----------	|----------	|------------------------------------------------------------	|---------------	|---------------------------------------	|
| Barcelona - Ciutadella 	| 2.1874    	| 41.3864  	| Parc de la Ciutadella                                      	| Ciutat Vella  	| Sant Pere, Santa Caterina i la Ribera 	|
| Barcelona - Eixample   	| 2.1538    	| 41.3853  	| Av. Roma - c/ Comte Urgell                                 	| Eixample      	| la Nova Esquerra de l'Eixample        	|
| Barcelona - Gràcia     	| 2.1534    	| 41.3987  	| Plaça Gal·la Placídia (Via Augusta - Travessera de Gràcia) 	| Gracia        	| la Vila de Gracia                     	|

##### Transport

- _bus_stops.csv_: paradas de bus según su posición y sus tipos.

| Code 	| Transport    	| Longitude 	| Latitude  	| Bus.Stop   	| District.Name  	| Neighborhood.Name         	|
|------	|--------------	|-----------	|-----------	|------------	|----------------	|---------------------------	|
| K014 	| Day bus stop 	| 2.171619  	| 41.413744 	| BUS -192-- 	| Horta-Guinardó 	| el Guinardó               	|
| K014 	| Day bus stop 	| 2.134902  	| 41.420222 	| BUS -124-- 	| Gràcia         	| Vallcarca i els Penitents 	|
| K014 	| Day bus stop 	| 2.162913  	| 41.423187 	| BUS -117-- 	| Horta-Guinardó 	| la Font d'en Fargues      	|

- _transports.csv_: transporte público (underground, Renfe, FGC, funicular, teleférico, tranvía, etc).

| Code 	| Transport   	| Longitude 	| Latitude  	| Station                                                	| District.Name       	| Neighborhood.Name      	|
|------	|-------------	|-----------	|-----------	|--------------------------------------------------------	|---------------------	|------------------------	|
| K001 	| Underground 	| 2.11937   	| 41.399203 	| FGC (L6) - REINA ELISENDA (Sortida Duquesa d'Orleans)- 	| Sarrià-Sant Gervasi 	| Sarrià                 	|
| K001 	| Underground 	| 2.135427  	| 41.397791 	| FGC (L6) - LA BONANOVA-                                	| Sarrià-Sant Gervasi 	| Sant Gervasi - Galvany 	|
| K001 	| Underground 	| 2.185391  	| 41.451492 	| METRO (L11) - CASA DE L'AIGUA (C. Vila-Real)-          	| Nou Barris          	| la Trinitat Nova       	|
