"""
Este script procesa los CSVs de inmigración y emigración en Dataframes ligeros,
con los datos necesarios y no más, que reduzcan la carga del frontend.
"""

import pandas as pd
import unidecode

traductor = {"Andalusia": "Andalucía",
             "Aragon": "Aragón",
             "Asturias": "Asturias",
             "Balearic Islands": "Islas Baleares",
             "Basque Country": "País Vasco",
             "Canary Islands": "Islas Canarias",
             "Cantabria": "Cantabria",
             "Castile and León": "Castilla y León",
             "Castilla-La Mancha": "Castilla-La Mancha",
             "Catalonia": "Cataluña",
             "Ceuta": "Ceuta y Melilla",
             "Community of Madrid": "Comunidad de Madrid",
             "Extremadura": "Extremadura",
             "Galicia": "Galicia",
             "La Rioja": "La Rioja",
             "Melilla": "Ceuta y Melilla",
             "Navarre": "Navarra",
             "Region of Murcia": "Región de Murcia",
             "Valencian Community": "Comuniadad Valenciana",
             "Abroad": "Internacional"
             }

# -- -- -- -- -- -- -- INMIGRACIÓN/EMIGRACIÓN -- -- -- -- -- -- --

# ---------- Tratamiento de los datos para los distritos ----------
# Importamos los datos
df_distritos_in_em = pd.read_csv("../../Dataset/files/immigrants_emigrants_by_destination2.csv").sort_values(by='weight', ascending=False)
# Lo convertimos a una tabla cruzada para facilitar los cálculos
distritos_in_em_crosstab = pd.crosstab(index=df_distritos_in_em['from'],
                                       columns=df_distritos_in_em['to'],
                                       values=df_distritos_in_em["weight"],
                                       aggfunc=lambda x: x)

# Creamos unos Dataframes con el nombre de distrito y los emigrantes/inmigrantes
nombres_distritos = list(pd.read_csv("/home/juan/Documentos/Master/Visualización de datos/Dataset/files/births.csv")["District Name"].unique())
nombres_distritos.remove("No consta")
col_distritos = []
col_count     = []
for column in distritos_in_em_crosstab:
    if column in nombres_distritos:
        col_distritos.append(column)
        col_count.append(distritos_in_em_crosstab[column].sum())
df_inmigrantes_distritos = pd.DataFrame({"distrito": col_distritos,
                                         "cuenta": col_count})
df_inmigrantes_distritos = df_inmigrantes_distritos.set_index('distrito')

col_distritos = []
col_count     = []
for _, row in distritos_in_em_crosstab.iterrows():
    if row.name in nombres_distritos:
        col_distritos.append(row.name)
        col_count.append(row.sum())
df_emigrantes_distritos = pd.DataFrame({"distrito": col_distritos,
                                        "cuenta": col_count})
df_emigrantes_distritos = df_emigrantes_distritos.set_index('distrito')

df_emigrantes_distritos.name  = "emigrantes según distrito"
df_inmigrantes_distritos.name = "inmigrantes según distrito"
df_emigrantes_distritos.to_csv("data/df_emigrantes_distritos.csv", header=None)
df_inmigrantes_distritos.to_csv("data/df_inmigrantes_distritos.csv",header=None)

# Guardamos también unos CSVs con los datos de inmigración y emigración de los
# distritos por separado para añadir en la info del hover

distritos_in_em_crosstab = distritos_in_em_crosstab.rename(index= traductor,
                                                           columns = traductor)
for nombre_distrito in nombres_distritos:
    # Inmigraciones (seleccionar columna con nombre del distrito)
    temp_df = distritos_in_em_crosstab[nombre_distrito]
    temp_df.dropna(inplace=True)
    temp_df = temp_df.groupby(temp_df.index) \
        .agg({nombre_distrito:sum})
    temp_df.to_csv("data/in_{}.csv".format(unidecode.unidecode(nombre_distrito)
                                           .replace("-", "_").replace(" ", "_")),
                   header=[nombre_distrito])

    # Emigraciones (seleccionar fila con nombre del distrito)
    temp_df = distritos_in_em_crosstab[distritos_in_em_crosstab.index == nombre_distrito]
    temp_df=temp_df.T.dropna()
    temp_df = temp_df.groupby(temp_df.index) \
        .agg({nombre_distrito:sum})
    temp_df.to_csv("data/em_{}.csv".format(unidecode.unidecode(nombre_distrito)
                                           .replace("-", "_").replace(" ", "_")))
# ------------- Tratamiento de los datos para las CCAA -------------

# Importamos los datos
df_ccaas_in_em = pd.read_csv("../../Dataset/files/immigrants_emigrants_by_destination.csv").sort_values(by='weight', ascending=False)
# Lo convertimos a una tabla cruzada para facilitar los cálculos
ccaas_in_em_crosstab = pd.crosstab(index=df_ccaas_in_em['from'],
                                   columns=df_ccaas_in_em['to'],
                                   values=df_ccaas_in_em["weight"],
                                   aggfunc=lambda x: x).T

# Creamos unos Dataframes con el nombre de distrito y los emigrantes/inmigrantes
col_ccaas = []
col_count   = []
for column in ccaas_in_em_crosstab:
    if column != "Abroad":  # Exlcuímos los de destino "Abroad"
        col_ccaas.append(column)
        col_count.append(ccaas_in_em_crosstab[column].sum())
df_inmigrantes_ccaas = pd.DataFrame({"distrito": col_ccaas,
                                         "cuenta": col_count})
df_inmigrantes_ccaas = df_inmigrantes_ccaas.set_index('distrito')
col_ccaas = []
col_count = []
for _, row in ccaas_in_em_crosstab.iterrows():
    if row.name != "Abroad":
        col_ccaas.append(row.name)
        col_count.append(row.sum())
df_emigrantes_ccaas = pd.DataFrame({"distrito": col_ccaas,
                                        "cuenta": col_count})
df_emigrantes_ccaas = df_emigrantes_ccaas.set_index('distrito')

df_emigrantes_ccaas.name  = "emigrantes según distrito"
df_inmigrantes_ccaas.name = "inmigrantes según distrito"
df_inmigrantes_ccaas = df_inmigrantes_ccaas[df_inmigrantes_ccaas.index != "Barcelona"]
df_emigrantes_ccaas = df_emigrantes_ccaas[df_emigrantes_ccaas.index != "Barcelona"]

# Traducimos a Español que es lo que sua el GeoJson

df_emigrantes_ccaas.index = df_emigrantes_ccaas.index.map(traductor)
df_inmigrantes_ccaas.index = df_inmigrantes_ccaas.index.map(traductor)

# Agrupamos Ceuta y Melilla en una sola fila
df_emigrantes_ccaas = df_emigrantes_ccaas.groupby(df_emigrantes_ccaas.index) \
    .agg({'cuenta':sum})
df_inmigrantes_ccaas = df_inmigrantes_ccaas.groupby(df_inmigrantes_ccaas.index) \
    .agg({'cuenta':sum})

df_emigrantes_ccaas.to_csv("data/df_emigrantes_ccaas.csv", header=None)
df_inmigrantes_ccaas.to_csv("data/df_inmigrantes_ccaas.csv",header=None)

# -- -- -- -- -- -- -- -- -- DESEMPLEO -- -- -- -- -- -- -- -- --

df_unemployment = pd.read_csv("../../Dataset/files/unemployment.csv")\
    .sort_values(by='Number', ascending=False)

#                       df_desempleo_distritos.csv
# Filtramos por año
df_unemployment = df_unemployment[df_unemployment["Year"] == 2017]
# Seleccionamos las columnas que nos interesan.
df_unemployment = df_unemployment[["District Name", "Number"]]
# Agrupamos por Distrito. Se convierte en index.
df_unemployment = df_unemployment.groupby(by = "District Name").sum()
# Quitamos el "No consta".
df_unemployment = df_unemployment[df_unemployment.index != "No consta"]
df_unemployment.to_csv("data/df_desempleo_distritos.csv", header=None)

#Generamos un csv por cada distrito para ver cuántos desempleados hay por barrio

df_unemployment = pd.read_csv("../../Dataset/files/unemployment.csv") \
    .sort_values(by='Number', ascending=False)
df_unemployment.index = df_unemployment["District Name"]
# Filtramos por año
df_unemployment = df_unemployment[df_unemployment["Year"] == 2017]
# Seleccionamos las columnas que nos interesan.
df_unemployment = df_unemployment[["Number",
                                   "Neighborhood Name"]]
df_unemployment = df_unemployment[df_unemployment.index != "No consta"]
df_unemployment = df_unemployment[df_unemployment["Neighborhood Name"] != "No consta"]

nombres_distritos = list(pd.read_csv("/home/juan/Documentos/Master/Visualización de datos/Dataset/files/births.csv")["District Name"].unique())
nombres_distritos.remove("No consta")

for nombre_distrito in nombres_distritos:
    temp_df = df_unemployment.loc[df_unemployment.index == nombre_distrito]
    temp_df = temp_df.groupby(by=temp_df["Neighborhood Name"]).sum()
    temp_df.reset_index(level=0, inplace=True)
    print(temp_df)
    temp_df.columns = ["in", nombre_distrito]
    temp_df.to_csv("data/un_{}.csv".format(unidecode.unidecode(nombre_distrito)
                                           .replace("-", "_").replace(" ", "_")),
                   index=False)