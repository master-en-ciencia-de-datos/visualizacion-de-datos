import pandas as pd
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 320)
import datetime

TRANSPORTS_CSV = "../../Dataset/files/transports.csv"
BUS_STOPS_CSV  = "../../Dataset/files/bus_stops.csv"
DEATSH_CSV     = "../../Dataset/files/deaths.csv"
ACCIDENTS_CSV  = "../../Dataset/files/accidents_2017.csv"

# ---------------- Procesamiento de los datos para el mapa ----------------

# Juntamos transports y bus_stops para facilitar el proceso
df_transports = pd.read_csv(TRANSPORTS_CSV)
df_bus_stops  = pd.read_csv(BUS_STOPS_CSV)
df_bus_stops = df_bus_stops.rename(columns={"Bus.Stop": "Station"})
df_transports = pd.concat([df_transports, df_bus_stops])

transports_colors_translator = {'Underground': "green",
                                'Railway (FGC)': "darkgreen",
                                'Tram': "lightgreen",
                                'Maritime station': "yellow",
                                'RENFE': "lime",
                                'Airport train': "greenyellow",
                                'Cableway': "sienna",
                                'Funicular': "chocolate",
                                'Day bus stop': "dodgerblue",
                                'Night bus stop': "stateblue",
                                'Airport bus stop': "deepskyblue",
                                'Bus station': "royalblue"}
transports_spanish_translator = {'Underground': "Metro",
                                'Railway (FGC)': "Tren (FGC)",
                                'Tram': "Tranvía",
                                'Maritime station': "Estación marítima",
                                'RENFE': "Tren (RENFE)",
                                'Airport train': "Tren al aeropuerto",
                                'Cableway': "Teleférico",
                                # 'Funicular': "Funicular",
                                'Day bus stop': "Bus diurno",
                                'Night bus stop': "Bus nocturno",
                                'Airport bus stop': "Bus al aeropuerto",
                                'Bus station': "Estación de autobuses"}
df_transports["Color"] = df_transports["Transport"].map(transports_colors_translator)
df_transports["Transport"] = df_transports["Transport"].map(transports_spanish_translator)
df_transports.to_csv("../../App/data/transport/transports.csv")

# Accidentes
month_translator_datetime = {'October': datetime.datetime(day=1, month=10, year=2017),
                             'September': datetime.datetime(day=1, month=9, year=2017),
                             'December': datetime.datetime(day=1, month=12, year=2017),
                             'July': datetime.datetime(day=1, month=7, year=2017),
                             'May': datetime.datetime(day=1, month=5, year=2017),
                             'June': datetime.datetime(day=1, month=6, year=2017),
                             'January': datetime.datetime(day=1, month=1, year=2017),
                             'April': datetime.datetime(day=1, month=4, year=2017),
                             'March': datetime.datetime(day=1, month=3, year=2017),
                             'November': datetime.datetime(day=1, month=11, year=2017),
                             'February': datetime.datetime(day=1, month=2, year=2017),
                             'August': datetime.datetime(day=1, month=8, year=2017)
                             }

weekday_translator = {'Monday': 1, 'Tuesday': 2, 'Wednesday': 3, 'Thursday': 4,
                      'Friday': 5, 'Saturday': 6, 'Sunday': 7}

part_of_day_translator = {'Afternoon': 'Tarde', 'Morning': 'Mañana', 'Night': 'Noche'}

df_accidents = pd.read_csv(ACCIDENTS_CSV)
df_accidents["Hour"] = pd.to_datetime(df_accidents["Hour"], format='%H')
df_accidents["Day"] = pd.to_datetime(df_accidents["Day"], format='%d')
df_accidents["Weekday"] = df_accidents["Weekday"].map(weekday_translator)
df_accidents["Month"] = df_accidents["Month"].map(month_translator_datetime)
df_accidents["Part of the day"] = df_accidents["Part of the day"].map(part_of_day_translator)

df_accidents.to_csv("../../App/data/transport/accidents.csv")