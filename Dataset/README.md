# Dataset

##### Docs

Breve descripción de los .csv del dataset.

##### Files

Archivos .csv que forman el dataset. No todos se usan en el dashboard.

##### Scripts

Distintos programas utilizados para la preparación y visualización del dataset junto con una carpeta de imágenes generadas en los _Jupyter notebooks_ para la memoria de procesado de datos.