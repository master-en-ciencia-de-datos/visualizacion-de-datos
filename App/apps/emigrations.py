"""
Script que representa un mapa coropléctico de los distintos distritos de la
ciudad de Barcelona según el número de emigrantes e inmigrantes.
"""

import pandas as pd
import json
from .scripts.util_geoJsonHelper import GeoJsonHelper
from .scripts.map_helpers        import  match_regions
from .scripts.plotly_helpers     import  plot_bar_and_line

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies      import Input, Output

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

# app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
from app import app

# ---------- Tratamiento de los datos para los distritos ----------
# Cargamos los datos
df_emigrantes_distritos  = pd.read_csv(
    "data/emigrations/df_emigrantes_distritos.csv",
    index_col=0, header=None)
df_inmigrantes_distritos = pd.read_csv(
    "data/emigrations/df_inmigrantes_distritos.csv",
    index_col=0, header=None)
df_unemployment = pd.read_csv("data/emigrations/df_desempleo_distritos.csv",
                              index_col=0, header=None)
df_total = pd.read_csv(
    "data/general/poblacion.csv",
    index_col=0)
# Cargamos el GeoJson con los distritos de la ciudad
with open('data/general/distritos.geojson') as f:
    geojson = json.load(f)
distritGeoJson = GeoJsonHelper(geojson)

# Creamos las correspondencias entre los nombres de los distritos de los datos
# y del geoJson
em_match_dict = match_regions(df_emigrantes_distritos.index,
                              distritGeoJson.distrits_names)
in_match_dict = match_regions(df_inmigrantes_distritos.index,
                              distritGeoJson.distrits_names)
# No los sobreescribimos
df_tmp_em = df_emigrantes_distritos.copy()
df_tmp_in = df_inmigrantes_distritos.copy()
# Renombramos
df_tmp_em.index = df_tmp_em.index.map(em_match_dict)
df_tmp_in.index = df_tmp_in.index.map(in_match_dict)
# Quitamos duplicados que hayan podido salir del fuzzy
df_tmp_em = df_tmp_em[~df_tmp_em.index.duplicated(keep=False)]
df_tmp_in = df_tmp_in[~df_tmp_in.index.duplicated(keep=False)]

# Reindexamos como en el geojson
df_reindexed_em_distrits = df_tmp_em.reindex(index = distritGeoJson.distrits_names)
df_reindexed_in_distrits = df_tmp_in.reindex(index = distritGeoJson.distrits_names)

# ------------- Tratamiento de los datos para las CCAA -------------

# Cargamos los datos
df_emigrantes_ccaas  = pd.read_csv("data/emigrations/df_emigrantes_ccaas.csv",
                                   index_col=0, header=None)
df_inmigrantes_ccaas = pd.read_csv("data/emigrations/df_inmigrantes_ccaas.csv",
                                   index_col=0, header=None)

# Cargamos el GeoJson con los ccaas de la ciudad
with open('data/general/ccaa.geojson') as f:
    geojson = json.load(f)
ccaaGeoJson = GeoJsonHelper(geojson)

# Creamos las correspondencias entre los nombres de los ccaas de los datos
# y del geoJson
em_match_dict = match_regions(df_emigrantes_ccaas.index,
                              ccaaGeoJson.distrits_names)
in_match_dict = match_regions(df_inmigrantes_ccaas.index,
                              ccaaGeoJson.distrits_names)
# No los sobreescribimos
df_tmp_em = df_emigrantes_ccaas.copy()
df_tmp_in = df_inmigrantes_ccaas.copy()
# Renombramos
df_tmp_em.index = df_tmp_em.index.map(em_match_dict)
df_tmp_in.index = df_tmp_in.index.map(in_match_dict)
# Quitamos duplicados que hayan podido salir del fuzzy
df_tmp_em = df_tmp_em[~df_tmp_em.index.duplicated(keep=False)]
df_tmp_in = df_tmp_in[~df_tmp_in.index.duplicated(keep=False)]

# Reindexamos como en el geojson
df_reindexed_em_ccaas = df_tmp_em.reindex(index = ccaaGeoJson.distrits_names)
df_reindexed_in_ccaas = df_tmp_in.reindex(index = ccaaGeoJson.distrits_names)

# -------- Tratamiento de los datos para el plot de barras y líneas --------

concat_df = pd.concat([df_emigrantes_distritos, df_inmigrantes_distritos, df_unemployment], axis=1)
concat_df.columns = ["Emigración", "Inmigración", "Desempleo"]
concat_df_normalized = concat_df.div(df_total["total"], axis=0)

# ------------------ Frontend de la aplicación ------------------
dropdown_options = [{"label": "Datos por Comunidad Autónoma", "value": "CCAA"},
                    {"label": "Datos por distrito", "value": "districts"}]
em_in_un         = ["Emigración", "Inmigración", "Desempleo"]
em_in            = ["Emigración", "Inmigración"]
layout = html.Div([
    html.Div([
        dcc.Markdown("Selecciona si quieres ver el mapa por distritos o por "
                     "Comunidades Autónomas"),
        dcc.Dropdown(
            id='mapa-value',
            options=dropdown_options,
            value="districts"
        ),
        dcc.Markdown("Selecciona qué métrica quieres representar"),
        dcc.RadioItems(
            id='eminun-value',
            options=[{'label': i, 'value': i} for i in em_in_un],
            value="Emigración",
            labelStyle={'display': 'inline-block'}
        ),
        dcc.Graph(id='choropleth-map')

    ],style={'width': '48%', 'float': 'left', 'display': 'inline-block'}),

    html.Div([
        dcc.Markdown("Selecciona la métrica y cómo representarla"),
        html.Div([
            dcc.Dropdown(
                id='selected-1-name',
                options=[{'label': i, 'value': i} for i in em_in_un] +
                        [{"label": "Ninguno", "value": "Ninguno"}],
                value="Emigración"
            ),
            dcc.RadioItems(
                id='selected-1-type',
                options=[{'label': i, 'value': i} for i in ["Barra", "Línea"]],
                value="Barra",
                labelStyle={'display': 'inline-block'}
            ),
        ], style={'columnCount': 2}),

        html.Div([
            dcc.Dropdown(
                id='selected-2-name',
                options=[{'label': i, 'value': i} for i in em_in_un] +
                        [{"label": "Ninguno", "value": "Ninguno"}],
                value="Inmigración"
            ),
            dcc.RadioItems(
                id='selected-2-type',
                options=[{'label': i, 'value': i} for i in ["Barra", "Línea"]],
                value="Barra",
                labelStyle={'display': 'inline-block'}
            ),
        ], style={'columnCount': 2}),

        html.Div([
            dcc.Dropdown(
                id='selected-3-name',
                options=[{'label': i, 'value': i} for i in em_in_un] +
                        [{"label": "Ninguno", "value": "Ninguno"}],
                value="Desempleo"
            ),
            dcc.RadioItems(
                id='selected-3-type',
                options=[{'label': i, 'value': i} for i in ["Barra", "Línea"]],
                value="Barra",
                labelStyle={'display': 'inline-block'}
            )
        ], style={'columnCount': 2}),
        dcc.Checklist(options=[
                {'label': 'Normalizar', 'value': 'normalizar'},
            ],
            value=['normalizar'],
            id="check-normalizar"),
        dcc.Markdown("Puedes reajustar los ejes haciendo doble click o arrastrando hacia arriba o hacia abajo!"),
        dcc.Markdown("Cuidado con el eje de desempleo. Es unas 10 veces mayor que el de inmigración y emigración 🧐"),
        dcc.Graph(id="bar-lines-plot")
    ],style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
])


@app.callback(
    dash.dependencies.Output('eminun-value', 'options'),
    [dash.dependencies.Input('mapa-value', 'value')])
def update_radio_options(mapa_value):
    chosen_eminun = em_in_un if mapa_value == "districts" else em_in
    return [{'label': i, 'value': i} for i in chosen_eminun]

@app.callback(
    dash.dependencies.Output('mapa-value', 'options'),
    [dash.dependencies.Input('eminun-value', 'value')])
def set_map_options(eminun_value):
    """ Si se elige Desempleo solo se puede escoger por distrito """
    if eminun_value == "Desempleo":
        # update_map("Desempleo", "districts")
        return [dropdown_options[1]]
    else:
        return dropdown_options

@app.callback(
    Output('choropleth-map', 'figure'),
    [Input('eminun-value', 'value'),
    Input('mapa-value', 'value')])

def update_map(emigration_or_inmigration_or_unemployment, mapa_value):
    print(emigration_or_inmigration_or_unemployment, mapa_value)
    usedGeoJson = distritGeoJson if mapa_value == "districts" else ccaaGeoJson

    if emigration_or_inmigration_or_unemployment == "Emigración":
        used_df = df_reindexed_em_distrits if mapa_value == "districts" \
            else df_reindexed_em_ccaas
        return usedGeoJson.generate_map(used_df,
                                        emigration_or_inmigration_or_unemployment,
                                        ccaa_or_districts=mapa_value)
    elif emigration_or_inmigration_or_unemployment == "Inmigración":
        used_df = df_reindexed_in_distrits if mapa_value == "districts" \
            else df_reindexed_in_ccaas
        return usedGeoJson.generate_map(used_df,
                                        emigration_or_inmigration_or_unemployment,
                                        ccaa_or_districts=mapa_value)
    else:
        used_df = df_unemployment
        return usedGeoJson.generate_map(used_df,
                                        emigration_or_inmigration_or_unemployment,
                                        ccaa_or_districts="districts")

@app.callback(
    Output('bar-lines-plot', 'figure'),
    [Input('selected-1-name', 'value'),
     Input('selected-1-type', 'value'),
     Input('selected-2-name', 'value'),
     Input('selected-2-type', 'value'),
     Input('selected-3-name', 'value'),
     Input('selected-3-type', 'value'),
     Input('check-normalizar', 'value')])
def update_bar_lines(selected_1_name, selected_1_type, selected_2_name,
                     selected_2_type, selected_3_name, selected_3_type,
                     check_normalizar):
    selected_df = concat_df_normalized if check_normalizar else concat_df
    normalizar = True if check_normalizar else False
    return plot_bar_and_line(selected_df, selected_1_name, selected_1_type,
                             selected_2_name, selected_2_type, selected_3_name,
                             selected_3_type, normalizar)
