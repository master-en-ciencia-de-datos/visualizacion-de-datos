"""
Script que representa en un mapa coropléctico de Barcelona los tipos de
transportes y accidentes de tráfico de ésta.
"""

import pandas as pd
import json
from .scripts.util_geoJsonHelper import GeoJsonHelper
from .scripts.plotly_helpers     import plot_horizontal_hist, plot_pie, plot_hist

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies      import Input, Output

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

# app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
from app import app

# Cargamos el GeoJson con los ccaas de la ciudad
with open('data/general/distritos.geojson') as f:
    geojson = json.load(f)
accidentsGeoJson = GeoJsonHelper(geojson)

ACCIDENTS_CSV  = "data/transport/accidents.csv"
df_accidents = pd.read_csv(ACCIDENTS_CSV)
TRANSPORTS_CSV  = "data/transport/transports.csv"
df_transports = pd.read_csv(TRANSPORTS_CSV)
unique_districts = list(df_transports["District.Name"].unique())

unique_districts.pop(3)  # quitamos el nan

fig_transports_total = plot_horizontal_hist(df_transports["Transport"])
fig_transports_total.update_layout(height=300, margin=dict(b=50, t=50),
                                   title="Número de estaciones de transporte en "
                                         "la ciudad de Barcelona")
layout = html.Div([
    html.Div([
        dcc.Markdown("Selecciona el área de los histogramas que deseas usar como filtro."),
        html.Div([
            dcc.Graph(
                id='weekday-hist',
                figure={
                    'data': [
                        {
                            'x': df_accidents['Weekday'],
                            'name': 'Weekday Histogram',
                            'type': 'histogram'
                        },
                    ],
                    'layout': {'height': 200,
                               'xaxis': {'tickformat': '%A',
                                         'ticktext': ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                                         'tickvals': [1, 2, 3, 4, 5, 6, 7]},
                               'dragmode': 'select',
                               'title': 'Día de la semana',
                               'margin': { 'l':50, 'r':50, 'b':50, 't':50, 'pad':4}
                               }
                }
            )


        ],style={'width': '25%', 'float': 'left', 'display': 'inline-block'}),
        html.Div([
            dcc.Graph(
                id='hour-hist',
                figure={
                    'data': [
                        {
                            'x': df_accidents['Hour'],
                            'name': 'Hour Histogram',
                            'type': 'histogram'
                        },
                    ],
                    'layout': {'height': 200,
                               'xaxis': {'tickformat': '%H'},
                               'dragmode': 'select',
                               'title': 'Hora',
                               'margin': { 'l':50, 'r':50, 'b':50, 't':50, 'pad':4}
                               }
                }
            )


        ],style={'width': '25%', 'float': 'left', 'display': 'inline-block'}),
        html.Div([
            dcc.Graph(
                id='day-hist',
                figure={
                    'data': [
                        {
                            'x': df_accidents['Day'],
                            'name': 'Day Histogram',
                            'type': 'histogram'
                        },
                    ],
                    'layout': {'height': 200,
                               'xaxis': {'tickformat': '%d',
                                         'tickvals': pd.date_range('1900-01-01', '1900-12-01', freq='W')},
                               'dragmode': 'select',
                               'title': 'Día',
                               'margin': { 'l':50, 'r':50, 'b':50, 't':50, 'pad':4}
                               }
                }
            )
        ],style={'width': '25%', 'float': 'center', 'display': 'inline-block'}),
        html.Div([
            dcc.Graph(
                id='month-hist',
                figure={
                    'data': [
                        {
                            'x': df_accidents['Month'],
                            'name': 'Month Histogram',
                            'type': 'histogram',
                        },
                    ],
                    'layout': {'height': 200,
                               'xaxis': {'tickformat': '%b',
                                         'tickvals': pd.date_range('2017-01', '2017-12', freq='MS')},
                               'dragmode': 'select',
                               'title': 'Mes',
                               'margin': { 'l':50, 'r':50, 'b':50, 't':50, 'pad':4}
                               }
                }
            )
        ],style={'width': '25%', 'float': 'right', 'display': 'inline-block'}),

    ],style={'width': '100%', 'display': 'inline-block'}),

    html.Div([
        dcc.Graph(id='transports-accidents-map'),
        html.Img(src=app.get_asset_url('transports-legend.png'), style={"width":"20%"})
    ],style={'width': '40%', 'float': 'left', 'display': 'inline-block'}),

    html.Div([
        dcc.Markdown("Selecciona el distrito para conocer sus transportes"),
        html.Div([
                dcc.Dropdown(id='dropdown-districts',
                             options=[{'label': v, 'value': v} for v in ["Total"] + unique_districts],
                             value="Total"),
                dcc.Graph(id="plot-transports-districts"),
        ]),

        html.Div([
            html.Div([
                dcc.Graph(id="plot-pie-accidents")
            ], className="four columns"),

            html.Div([
                dcc.Graph(id="plot-bar-injuries")
            ], className="four columns"),
            html.Div([
                dcc.Graph(id="plot-bar-vehicles-victims")
            ], className="four columns"),
        ], className="row")
        ],style={'width': '60%', 'float': 'right', 'display': 'inline-block',
                 }),
])

@app.callback(
    Output('transports-accidents-map', 'figure'),
    [Input('weekday-hist', 'selectedData'),
     Input('hour-hist', 'selectedData'),
     Input('day-hist', 'selectedData'),
     Input('month-hist', 'selectedData')])

def update_map(selected_weekday, selected_hour, selected_day, selected_month):
    return accidentsGeoJson.generate_accidents_map(df_accidents, selected_weekday, selected_hour, selected_day, selected_month)

@app.callback(
    [Output('plot-transports-districts', 'figure'),
    Output('plot-bar-injuries', 'figure'),
    Output('plot-bar-vehicles-victims', 'figure'),
    Output('plot-pie-accidents', 'figure')],
    [Input('dropdown-districts', 'value')])
def update_plots(district):
    if district == "Total":
        temp_df_transports = df_transports
        temp_df_accidents = df_accidents
    else:
        temp_df_transports = df_transports[df_transports["District.Name"] == district]
        temp_df_accidents = df_accidents[df_accidents["District Name"] == district]
    fig_transports = plot_horizontal_hist(temp_df_transports["Transport"])
    fig_transports.update_layout(
        height=300,
        margin=dict(
            b=50,
            t=50,
        ),
        title="Estaciones de transporte"
    )

    fig_accidents_injuries = plot_hist([temp_df_accidents["Mild injuries"],
                                        temp_df_accidents["Serious injuries"]],
                                       ["Lesiones leves", "Lesiones graves"])
    fig_accidents_injuries.update_layout(
        height=300,
        margin=dict(
            l=5,
            r=5,
            b=50,
            t=50,
        ),
        title="Lesiones"
    )

    fig_accidents_vehicles_victims = plot_hist([temp_df_accidents["Vehicles involved"],
                                                temp_df_accidents["Victims"]],
                                               ["Vehículos involucrados","Víctimas"])
    fig_accidents_vehicles_victims.update_layout(
        height=300,
        margin=dict(
            l=5,
            r=30,
            b=50,
            t=50,
        ),
    )

    fig_pie_accidents = plot_pie(temp_df_accidents["Part of the day"])
    fig_pie_accidents.update_layout(
        height=300,
        margin=dict(
            l=30,
            r=5,
            b=50,
            t=50,
        ),
        title="Momento del accidente"
    )
    return fig_transports, fig_accidents_injuries, \
           fig_accidents_vehicles_victims,fig_pie_accidents
