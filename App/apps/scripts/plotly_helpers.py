import plotly.graph_objects as go

def plot_hist(columnas, columnas_name=None):
    """
    Crea un histograma de varias series de pandas con las categorías en
    horizontal.
    :param columnas_name: label que saldrá en la leyenda.
    :param columnas:
    :return:
    """
    fig = go.Figure()
    for i, columna in enumerate(columnas):
        if columnas_name is not None:
            name=columnas_name[i]
        else: name=None
        fig.add_trace(go.Histogram(x=columna, name=name))
    fig.update_yaxes(categoryorder="total ascending")
    return fig

def plot_horizontal_hist(columna):
    """
    Crea un histograma de una serie de pandas con las categorías en vertical.
    :param columna:
    :return:
    """
    fig = go.Figure(data=[go.Histogram(y=columna)])
    fig.update_yaxes(categoryorder="total ascending")
    return fig

def plot_pie(columna):
    """
    Crea un gráfico tipo tarta de una serie de pandas.
    :param columna:
    :return:
    """
    temp_df = columna.value_counts()
    fig = go.Figure(data=[go.Pie(labels=temp_df.index.values.tolist(),
                                 values=temp_df.values.tolist())])
    # fig.update_yaxes(categoryorder="total ascending")
    return fig

def plot_bar_and_line(df, selected_1_name, selected_1_type, selected_2_name,
                      selected_2_type, selected_3_name, selected_3_type,
                      normalizar=False):
    """
    Representa un gráfico de barras y líneas según las entradas.
    """

    all_names = [selected_1_name, selected_2_name, selected_3_name]
    all_types = [selected_1_type, selected_2_type, selected_3_type]
    if selected_1_name != "Ninguno": df=df.sort_values(by=selected_1_name)
    fig = go.Figure()
    for (name, _type) in zip(all_names, all_types):
        if name != "Ninguno":
            go_fun = go.Bar if _type == "Barra" else go.Scatter
            fig.add_trace(
                go_fun(x=df.index, y=df[name],
                       name=name,
                       yaxis="y2" if name == "Desempleo" else "y1",
                       opacity=0.35 if name == "Desempleo" else None,
                       marker_color='#1f77b4' if name == "Desempleo" else None)
            )

    if "Emigración" in all_names and "Inmigración" in all_names:
        temp_title = "Emigrantes o Inmigrantes"
    elif "Emigración" in all_names and "Inmigración" not in all_names:
        temp_title = "Emigrantes"
    elif "Emigración" not in all_names and "Inmigración" in all_names:
        temp_title = "Inmigrantes"
    else:
        temp_title = None
    if normalizar: temp_title = "Proporción " + temp_title
    fig.update_layout(
        legend_orientation="h",
        legend=dict(x=0.1, y=1.2),
        yaxis=dict(
            title=temp_title,
            titlefont=dict(
                color="#000000"
            ),
            tickfont=dict(
                color="#000000"
            ),
            # range=[0, 1] if normalizar else None,
        ),

        yaxis2=dict(
            title="Personas desempleadas",
            titlefont=dict(
                color="#1f77b4"
            ),
            tickfont=dict(
                color="#1f77b4"
            ),
            anchor="free",
            overlaying="y",
            side="right",
            position=1,
            # range=[0, 1] if normalizar else None,
        ) if "Desempleo" in all_names
        else None
    )
    fig.update_layout(barmode='group', hovermode = 'x')

    return fig