import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

def print_uniques(df, max_print=5):
    """
    Imprime el nombre de la columna y sus valores únicos.
    :param df: dataframe sobre el que se trabaja
    :param max_print: número máximo de elementos que se imprimen
    :return:
    """
    print("\n ----- Valores únicos ----- \n")
    for (columnName, columnData) in df.iteritems():
        if len(columnData.unique()) <= max_print:
            print(columnName, ': ', columnData.unique())
        else:
            print(columnName, ': ', columnData.unique()[0:max_print], " ...")
    return