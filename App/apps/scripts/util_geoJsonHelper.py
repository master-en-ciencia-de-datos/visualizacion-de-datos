import numpy as np
import copy
from .map_helpers import *
import pandas as pd
import os

MAPBOX_APIKEY = os.environ.get('MAPBOX_APIKEY')
if MAPBOX_APIKEY is None:
    from secrets import MAPBOX_APIKEY
df_transports = pd.read_csv("data/transport/transports.csv")

class GeoJsonHelper:
    """
    Extrae la información necesaria para pintar un mapa coropléctico.
    """
    def __init__(self, geojson):
        self.geojson = geojson

        # Número total de distritos/CCAA
        self.n_distrits = len(geojson['features'])
        # print("El GeoJson tiene {} distritos/CCAA".format(self.n_distrits))
        # Nombre de los distritos/CCAA
        try:
            self.distrits_names = [geojson['features'][k]['properties']['n_distri']
                                   for k in range(self.n_distrits)]
        except KeyError:
            self.distrits_names = [geojson['features'][k]['properties']['name_1']
                                   for k in range(self.n_distrits)]

    def get_centers(self):
        """
        Calcula la longitud y latitud los centros de los distritos.
        :return: (longitud, latitud)
        """
        lon, lat =[], []

        for k in range(self.n_distrits):
            geometry = self.geojson['features'][k]['geometry']

            if geometry['type'] == 'Polygon':
                coords=np.array(geometry['coordinates'][0])
            elif geometry['type'] == 'MultiPolygon':
                # coords=np.array(geometry['coordinates'][0][0])
                if len(np.shape(geometry['coordinates'])) == 2:
                    # El polígono tiene shape de 4 valores (1, 1, X, 2). Hay que
                    # iterar sobre las coordinadas hasta el contrar el polígono
                    # mayor.
                    temp_shape_list = []
                    for sub_coordinates in geometry['coordinates']:
                        temp_shape_list.append(np.shape(sub_coordinates)[1])
                    max_ind = temp_shape_list.index(max(temp_shape_list))
                    coords = np.array(geometry['coordinates'][max_ind][0])
                elif len(np.shape(geometry['coordinates'])) == 4:
                    coords = np.array(geometry['coordinates'][0][0])

                else:
                    total_coords = []
                    for sub_coords in geometry['coordinates']:
                        total_coords.append(sub_coords[0])
                    coords = np.concatenate(total_coords)
            else:
                print("Geometría desconocida")
                continue
            # centroides
            lon.append(sum(coords[:,0]) / len(coords[:,0]))
            lat.append(sum(coords[:,1]) / len(coords[:,1]))

        return lon, lat

    def make_sources(self, downsample = 10):
        """
        Reduce el número de puntos del polígono para acelerar el renderizado
        del mapa.
        :param downsample: número de veces que se reduce la calidad del polígono
        :return: lista de diccionarios de feautures del geoJson.
        """
        sources = []
        geojson_features = copy.deepcopy(self.geojson['features'])

        for feature in geojson_features:

            if downsample > 0:
                # coords = np.array(feature['geometry']['coordinates'][0][0])
                # Cogiendo el [0][0] puede haber problemas en geoJson que separen
                # las provincias en varios polígonos. Hay que coger el mayor polígono.

                if len(np.shape(feature['geometry']['coordinates'])) == 2:
                    # El polígono tiene shape de 4 valores (1, 1, X, 2). Hay que
                    # iterar sobre las coordinadas hasta el contrar el polígono
                    # mayor.
                    temp_shape_list = []
                    for sub_coordinates in feature['geometry']['coordinates']:
                        temp_shape_list.append(np.shape(sub_coordinates)[1])
                    max_ind = temp_shape_list.index(max(temp_shape_list))
                    coords = feature['geometry']['coordinates'][max_ind][0]
                    coords = coords[::downsample]
                elif len(np.shape(feature['geometry']['coordinates'])) == 4:
                    coords = np.array(feature['geometry']['coordinates'][0][0])
                    coords = coords[::downsample]
                else:
                    total_coords = []
                    for sub_coords in feature['geometry']['coordinates']:
                        total_coords.append(sub_coords[0][::downsample])
                    coords = np.concatenate(total_coords)

                # coords = coords[::downsample]
                feature['geometry']['coordinates'] = [[coords]]

            sources.append(dict(type = 'FeatureCollection',
                                features = [feature])
                           )
        return sources

    def generate_map(self, df, emigration_or_inmigration_or_unemployment,
                    ccaa_or_districts="districts"):
        """
        Genera el mapa según el Dataframe df para los distritos o las CCAA.
        :param ccaa_or_districts: si se va a representar por distritos o por
        CCAA.
        :param emigration_or_inmigration_or_unemployment: "Emigración" o
        "Inmigración" o "Desempleo" para cambiar el hover.
        :param df: Dataframe con el que se genera el mapa.
        :return: devuelve la figura.
        """
        # Generamos los datos necesarios para la representación del mapa
        colormap = 'Blues'

        cmin = df[1].min()
        cmax = df[1].max()
        cmax = 6000 if ccaa_or_districts == "CCAA" else df[1].max()
        sources = self.make_sources(downsample=10)
        lons, lats = self.get_centers()

        sm = scalarmappable(colormap, cmin, cmax)
        scatter_colors = get_scatter_colors(sm, df[1])
        colorscale = get_colorscale(sm, df[1], cmin, cmax, ccaa_or_districts)
        hover_text = get_hover_text_emigrations(df, emigration_or_inmigration_or_unemployment,
                                                ccaa_or_districts)
        tickformat = "" # Cómo se muestran los datos en la leyenda del mapa
        if ccaa_or_districts == "CCAA":
            suptitle = emigration_or_inmigration_or_unemployment \
                       + " en Barcelona según la CCAA"
            lat  = 40.416775
            lon  = -3.703790
            zoom = 4.1
        elif ccaa_or_districts == "districts":
            suptitle = emigration_or_inmigration_or_unemployment \
                       + " en los distritos de Barcelona"
            lat  = 41.39500
            lon  = 2.143337
            zoom = 10.5
        else:
            suptitle = emigration_or_inmigration_or_unemployment + " en ?"
            lat  = 40.416775
            lon  = -3.703790
            zoom = 4.2

        data = dict(type='scattermapbox',
                    lat=lats,
                    lon=lons,
                    mode='markers',
                    text=hover_text,
                    marker=dict(size=1,
                                color=scatter_colors,
                                showscale = True,
                                cmin = df[1].min(),
                                cmax = 6000 if ccaa_or_districts == "CCAA" else df[1].max(),
                                colorscale = colorscale,
                                colorbar = dict(tickformat = tickformat, title=emigration_or_inmigration_or_unemployment)
                                ),
                    showlegend=False,
                    hoverinfo='text',
                    )
        layers=([dict(sourcetype = 'geojson',
                      source =sources[k],
                      below="",
                      type = 'line',    # bordes
                      line = dict(width = 1),
                      color = 'black',
                      ) for k in range(self.n_distrits)
                 ] +

                [dict(sourcetype = 'geojson',
                      source =sources[k],
                      below="water",
                      type = 'fill',   # area dentro de los bordes
                      color = scatter_colors[k],
                      opacity=0.8
                      ) for k in range(self.n_distrits)
                 ]
                )
        layout = dict(title=suptitle,
                      autosize=False,
                      width=600,
                      height=600,
                      hovermode='closest',
                      hoverdistance = 50,

                      mapbox=dict(accesstoken=MAPBOX_APIKEY,
                                  layers=layers,
                                  bearing=0,
                                  center=dict(
                                      lat=lat,  # centro del mapa
                                      lon=lon),
                                  pitch=0,
                                  zoom=zoom,
                                  style = 'light'
                                  )
                      )
        fig = dict(data=[data], layout=layout)
        return fig

    def generate_accidents_map(self, df_accidentes, weekday, hour, day, month):
        temp_df = df_accidentes.copy()
        for column, option in zip(["Weekday", "Hour", "Day", "Month"],
                                  [weekday, hour, day, month]):
            if option is not None:
                date_to, date_from = option['range']['x'][0], option['range']['x'][1]
                print(date_from, date_to)
                if column == "Weekday":
                    # en este caso from y to estan al reves
                    # en el histograma 4.5 es viernes, 5.5 sabado...
                    chosen_weekdays = list(range(int(np.floor(date_to + 1)),
                                                 int(np.floor(date_from))+1))
                    print(chosen_weekdays)
                    temp_df = temp_df[temp_df[column].isin(chosen_weekdays)]
                    print(len(temp_df))
                else:
                    temp_df = temp_df[(temp_df[column] > date_from)
                                      & (temp_df[column] < date_to)]

        sources = self.make_sources(downsample=10)
        lons, lats = temp_df["Longitude"], temp_df["Latitude"]
        lat  = 41.394873
        lon  = 2.143337
        zoom = 10.9
        # hover_text = get_hover_text(df)
        tickformat = "" # Cómo se muestran los datos en la leyenda del mapa
        data = [dict(type='scattermapbox',
                     lat=lats,
                     lon=lons,
                     mode='markers',
                     marker=dict(size=10,
                                 color="red",
                                 opacity=0.05,
                                 ),
                     showlegend=False,
                     hoverinfo='text',
                     text=get_hover_text_transport(df_accidentes, "accidents")
                     )] \
               + [ dict(type='scattermapbox',
                        lat=df_transports["Latitude"],
                        lon=df_transports["Longitude"],
                        mode='markers',
                        marker=dict(size=6,
                                    color=df_transports["Color"],
                                    opacity=1,
                                    ),
                        showlegend=False,
                        hoverinfo='text',
                        text=get_hover_text_transport(df_transports, "transports"),
                        )
                   ]

        layers=([dict(sourcetype = 'geojson',
                      source =sources[k],
                      below="",
                      type = 'line',    # bordes
                      line = dict(width = 1),
                      color = 'black',
                      ) for k in range(self.n_distrits)
                 ]
        )
        layout = dict(title="Accidentes y transportes",
                      autosize=False,
                      width=600,
                      height=650,
                      hovermode='closest',
                      hoverdistance = 10,

                      mapbox=dict(accesstoken=MAPBOX_APIKEY,
                                  layers=layers,
                                  bearing=0,
                                  center=dict(
                                      lat=lat,  # centro del mapa
                                      lon=lon),
                                  pitch=0,
                                  zoom=zoom,
                                  style = 'light'
                                  ),
                      margin={'l':50, 'r':50, 'b':50, 't':50, 'pad':4},
                      )
        fig = dict(data=data, layout=layout)
        return fig
