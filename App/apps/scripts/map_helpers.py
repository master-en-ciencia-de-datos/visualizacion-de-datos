"""Funciones que utilza GeoJsonHelper"""

import pandas as pd
import numpy as np
from fuzzywuzzy import fuzz, process
from matplotlib.colors import Normalize
from matplotlib import cm
import unidecode
import copy

WEEKDAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

def match_regions(list1, list2):
    """
    Calcula un diccionario con las mejores coincidencias para dos listas. En
    este caso se usa para relacionar los distritos de los datos con los del
    geojson, ya que pueden tener alguna letra distinta.
    de strings.
    :param list1: primera lista a comparar.
    :param list2: segunda lista a comparar.
    :return: {'string1_1': 'string2_1', 'string1_2': 'string2_2', ...}
    """
    # Solo cogemos la mejor coincidencia
    matched = [process.extract(list1[i], list2, limit=1,
                               scorer=fuzz.partial_ratio)[0][0]
               for i in range(0,len(list1))]

    return {key: value for (key, value) in zip(list1, matched)}

def scalarmappable(cmap, cmin, cmax):
    """
    Crea un mapa de color escalado entre cmin y cmax.
    :param cmap: mapa de color a normalizar.
    :param cmin: mínimo valor del mapa de color.
    :param cmax: máximo valor del mapa de color.
    :return: scarlarMap.
    """
    colormap = cm.get_cmap(cmap)
    norm = Normalize(vmin=cmin, vmax=cmax)
    return cm.ScalarMappable(norm=norm, cmap=colormap)

def get_scatter_colors(sm, df):
    """
    Genera un color para cada punto según su valor en el dataframe y el mapa
    de color $sm. Pinta en gris los puntos que sin valores en el Dataframe.
    :param sm: scalarMap sobre el que se trabaja.
    :param df: Dataframe con los valores que indican la intensidad del color.
    :return: ['rgba(x1,y1,z1,1)', 'rgba(x2,y2,z2,1)', ...]
    """
    grey = 'rgba(128,128,128,1)'
    return ['rgba' + str(sm.to_rgba(m, bytes = True, alpha = 1)) if not np.isnan(m) else grey for m in df]

def get_colorscale(sm, df, cmin, cmax, ccaa_or_districts):
    """
    Genera una escala de colores según el Dataframe df y los límites inferior y
    superior cmin y cmax para varios percentiles del mapa de color sm. Por
    ejemplo:
        # El 10% más bajo (0.1) de los valores tienen el color rgb(0, 0, 0)
        [0, "rgb(0, 0, 0)"],
        [0.1, "rgb(0, 0, 0)"],

        # Los valores entre 10-20% del cmin y cmax tienen el rgb(20, 20, 20)
        [0.1, "rgb(20, 20, 20)"],
        [0.2, "rgb(20, 20, 20)"],
        etcétera.
    Si es CCAA hacemos personalizada.
    :param sm: scalarmap sobre el que se trabaja.
    :param df: Dataframe con los valores que se traducirán en colores.
    :param cmin: valor mínimo del Dataframe.
    :param cmax: valor máximo del Dataframe.
    :return: lista de listas de colores con el percentil y su color
    correspondiente.
    """
    xrange = np.linspace(0, 1, len(df))
    if ccaa_or_districts == "districts":
        values = np.linspace(cmin, cmax, len(df))
    else:
        # values = np.linspace(cmin, cmax, len(df))
        values = np.linspace(cmin, cmax, len(df))

    return [[i, 'rgba' + str(sm.to_rgba(v, bytes = True))] for i,v in zip(xrange, values) ]

def get_hover_text_emigrations(df, emigration_or_inmigration, ccaa_or_districts="districts"):
    """
    Genera el texto que se muestra cuando se pasa el ratón por el mapa.
    :param ccaa_or_districts: lo usamos para añdir infor de qué barrio a
    qué barrio se mueve la gente.
    :param emigration_or_inmigration: "Emigración" o "Inmigración" para poner
    en el texto.
    :param df: Dataframe con los datos que se van a mostrar.
    :return: lista con los datos por punto del mapa.
    """

    text_value = (df[1]).astype(int).astype(str)
    if emigration_or_inmigration == "Emigración":
        with_data = '<b>{}</b> <br> {} emigrantes'
    elif emigration_or_inmigration == "Inmigración":
        with_data = '<b>{}</b> <br> {} inmigrantes'
    elif emigration_or_inmigration == "Desempleo":
        with_data = '<b>{}</b> <br> {} desempleados'
        # return [with_data]
    else:
        with_data = '<b>{}</b> <br> no data'
    no_data = '<b>{}</b> <br> no data'

    hover_text = []
    for p,v in zip(df.index, text_value):
        new_with_data = copy.copy(with_data)
        new_with_data += " en total<br> "
        if ccaa_or_districts == "districts":
            if emigration_or_inmigration == "Emigración":
                preffix = "em"
            elif emigration_or_inmigration == "Inmigración":
                preffix = "in"
            else:
                preffix = "un"
            temp_df_name = "data/emigrations/{}_{}.csv".format(preffix,
                                                   unidecode.unidecode(p)
                                                   .replace("-", "_").replace(" ", "_"))
            temp_df = pd.read_csv(temp_df_name).sort_values(by=[p],
                                                            ascending=False)
            for _, row in temp_df.iterrows():
                if emigration_or_inmigration == "Emigración":
                    new_with_data += "<br> {} hacia {}".format(int(row[p]),
                                                               row["to"])
                elif emigration_or_inmigration == "Inmigración":
                    new_with_data += "<br> {} de {}".format(int(row[p]),
                                                            row["from"])
                else:
                    new_with_data += "<br> {} de {}".format(int(row[p]),
                                                            row["in"])
        hover_text.append(new_with_data.format(p,v) if v != 'nan%' else no_data.format(p))
    return hover_text


def get_hover_text_transport(df, accidents_or_transport):
    if accidents_or_transport == "accidents":
        with_data = '<b>Accidente {}</b> <br> Lesiones leves: {} <br> Lesiones ' \
                    'graves: {} <br> Víctimas: {} <br> Vehículos implicados: {}'
        mild_value = (df["Mild injuries"]).astype(int).astype(str)
        serious_value = (df["Serious injuries"]).astype(int).astype(str)
        victims_value = (df["Victims"]).astype(int).astype(str)
        vehicles_value = (df["Vehicles involved"]).astype(int).astype(str)
        return [with_data.format(p, m, s, v, ve)
                for p, m, s, v, ve in zip(df.index, mild_value, serious_value,
                                          victims_value, vehicles_value)]

    elif accidents_or_transport == "transports":
        with_data = '<b>Transporte {}</b> <br> Tipo: {} <br> Parada: {}'
        return [with_data.format(p, t, s)
                for p, t, s in zip(df.index, df["Transport"], df["Station"])]
    return