import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from app import app, server
from apps import emigrations, transport

app.layout = html.Div(
    [
        # header
        html.Div([

            # html.Span("Visualización de datos", className='app-title'),

            html.Div([
                # html.Img(src=app.get_asset_url('banner.jpg'), style={'height':'50%', 'width':'50%'})]
                html.H1('Conociendo Barcelona por sus datos', style={"float":"right"})]
                ,style={"float":"left","width":"50%", 'display': 'inline-block'}),

            html.Div([
                html.Img(src=app.get_asset_url('icon.png'), style={'height':'10%', 'width':'10%', "float":"left"}),
                html.A("Gitlab", href="https://gitlab.com/master-en-ciencia-de-datos/visualizacion-de-datos", style={"font-size": "2.0em"})]
                ,style={"float":"right","width":"48%", 'display': 'inline-block'}),
        ],
            className="row header"
        ),

        # tabs
        html.Div([

            dcc.Tabs(
                id="tabs",
                style={"height":"20","verticalAlign":"middle"},
                children=[
                    dcc.Tab(label="Emigración, inmigración y desempleo", value="emigrations_tab"),
                    dcc.Tab(label="Accidentes y transportes", value="transport_tab"),
                ],
                value="emigrations_tab",
            )

        ],
            className="row tabs_div"
        ),

        # Tab content
        html.Div(id="tab_content", className="row", style={"margin": "2% 3%"}),
    ],
    className="row",
    style={"margin": "0%"},
)

@app.callback(Output("tab_content", "children"), [Input("tabs", "value")])
def render_content(tab):
    if tab == "transport_tab":
        return transport.layout
    elif tab == "emigrations_tab":
        return emigrations.layout
    else:
        return emigrations.layout

if __name__ == "__main__":
    app.run_server(host='0.0.0.0', debug=False, port=8050)