# Dashboard para la visualización de los datos

Existen dos vías para probar el dashboard en local. La primera sería instalando lo necesario en el PC y la segunda alojando el dashboard en un contenedor de [Docker](https://www.docker.com/). Al haber hecho público el repositorio se ha eliminado la _API key_ para que el usuario que clone el repositorio la sustituya por la [suya propia](https://docs.mapbox.com/help/glossary/access-token) en [esta](https://gitlab.com/master-en-ciencia-de-datos/visualizacion-de-datos/-/blob/master/App/apps/scripts/util_geoJsonHelper.py#L7) línea.

## Instalación en local

Se debe clonar el repositorio, acceder al directorio [App](../App) e instalar los paquetes necesarios con:

```bash
pip3 install -r requirements.txt
```

Se recomienda usar un entorno virtual.

Una vez instalado simplemente hay que ejecutar `python3 -m index` o `python3 index.py`.

La carpeta [App](../App) está preparada para desplegar directamente desde [Heroku](https://www.heroku.com/).

## Contenedor Docker

Para construir el contenedor basta con tener instalado [Docker](https://docs.docker.com/get-docker/) en nuestro PC y ejecutar los siguientes comandos:

```bash
cd /path/to/App  # En caso de no estar en la carpeta App
sudo docker build --tag barcelona-dashboard:1.0 .
sudo docker run --publish 8050:8050 --detach --name bcn-container barcelona-dashboard:1.0
```

y ya tendríamos en http://0.0.0.0:8050/ el dashboard accesible. Para solventar problemas con el contenedor se puede usar `sudo docker logs bcn-container` o publicar un _Issue_ en el repositorio.

En Windows y MacOS sería similar, simplemente habría que ignorar el `sudo`.
